// pages/plant.js
let storage = require('../../wechat-lib/storage');
let common = require('../../wechat-lib/common');
let app=getApp();
Page({
  mixins: [require('../../wechat-lib/themeChanged')],
  /**
   * 页面的初始数据
   */
  data: {
    image: '/static/logo.png',
    selectIndex: 0
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (app.globalData.imgSrc) {
      this.setData({
        image: app.globalData.imgSrc,
        select: 1
      })
      app.globalData.imgSrc=undefined;
      wx.showToast({
        "title": '点击图片识别',
        "duration": 1000
      })
    }
  },
  priview: function () {
    let that = this
    if (that.data.selectIndex != 0) {
      wx.previewImage({
        current: that.data.image,
        urls: [that.data.image],
      })
    }
  },
  selectImage: function (res) {
    storage.takePicture(res).then(res => {
      this.setData({
        image: res,
        selectIndex: 1
      })
    }, res => {
      res && wx.showToast({
        title: res,
        icon: "error"
      })
    })
  },
  uploadImage: function () {
    storage.uploadFile(this.data.image, {}).then(
      success=>{
        wx.navigateTo({
          url: '/pages/plant/result?url=' + encodeURIComponent(success.url),
        })
      },
      err=>{
        err && common.showErrorMsg(err)
      }
    )
    
  },
  cropper:function(){
    let that=this;
    wx.navigateTo({
      url: 'cropperpage?url='+encodeURIComponent(that.data.image),
    })
  },
  goresult: () => {
    wx.navigateToMiniProgram({
      appId: "wx1ef4cc1d363a714d"
    })
  },
  onShareAppMessage: () => {
    return {
      title: "花草鉴定",
      desc: "帮你认识各种花花草草。不认识的植物，扫一下立马识别。"
    }
  },
  onShareTimeline: () => {
    return {
      title: "花草鉴定，帮你认识各种花花草草。",
      query: "",
    }
  }
})