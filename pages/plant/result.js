// pages/plant/result.js
let app=getApp();
let request=require("../../wechat-lib/request")
Page({
  mixins: [require('../../wechat-lib/themeChanged')],
  /**
   * 页面的初始数据
   */
  data: {
    result:null,
    autoplay:true,
    showinfo:[],
    originImage:'/static/logo.png'
  },
  onLoad:function(options){
    this.setData({
      url:decodeURIComponent(options.url)
    })
    // console.log(options)
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow:async function () {
    // if (this.data.url) {
    //   // this.setData({
    //   //   url: app.globalData.url,
    //   //   originImage:app.globalData.originImage
    //   // })
    //   app.globalData.url=undefined
    //   app.globalData.originImage=undefined
    // }
    let that=this
    if(!that.data.result){
      const response=await request.yunrequest("wxv2", "getPlant",{picture:that.data.url})
      console.log(response)
      that.setData({
        result:response.result,
        detail:response.result[0].baike_info.description
      })
      console.log(response)
    }
  },
  showinfo:function (e){
    let that=this
    console.log(e)
    that.setData({
      detail:e.currentTarget.dataset.description
    })
  },
  bindchange:function(e){
    console.log(e.detail.current)
    let that=this;
    console.log(that.data.result[e.detail.current])
    that.setData({
      detail:that.data.result[e.detail.current].baike_info.description?that.data.result[e.detail.current].baike_info.description:''
    })
  },
  tapImage:function(e){
    console.log(e);
    if( e.currentTarget.dataset.src){
      wx.previewImage({
        urls: [e.currentTarget.dataset.src]
      })
    }
  },
  onShareAppMessage:()=>{
    return {
      title:"花草鉴定",
      desc:"使用了花草鉴定,识别植物真准，认识的，不认识的统统都能搞定。"
    }
  },
  onShareTimeline:function(){
    return {
      title:"使用了花草鉴定,识别植物真准，认识的，不认识的统统都能搞定。",
      query:"url="+encodeURIComponent(this.data.url),
    }
  }
})