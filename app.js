//app.js
require('./wechat-lib/Mixins.js');
const themeListeners = [];
App({
  globalData: {
    theme: 'light', // dark
    statusBarHeight:0,
    themeListeners:undefined,
    userInfo: null,
    fileId:null
  },
  onLaunch: function () {
    
    const  systenInfo=wx.getSystemInfoSync()
    this.globalData.theme=systenInfo.theme;
    this.globalData.statusBarHeight=systenInfo.statusBarHeight;
  },
  onThemeChange:function(res){
    this.globalData.theme=res.theme
    console.log("Theme Change",res.theme)
    if(this.globalData.themeListeners!=undefined){
      this.globalData.themeListeners(this.globalData.theme);
    }
  },
  watchThemeChange(listener) {
    this.globalData.themeListeners=listener;
  },
  unWatchThemeChange(listener) {
    this.globalData.themeListeners=undefined;
  }
})